//inject the twitterService into the controller
app.controller('TwitterController', function($scope,$q, twitterService) {

    $scope.tweets=[]; //array of tweets

    twitterService.initialize();

    //using the OAuth authorization result get the latest 20 tweets from twitter for the user
    $scope.refreshTimeline = function(maxId) {
        twitterService.getLatestTweets(maxId).then(function(data) {
            $scope.tweets = $scope.tweets.concat(data);
            $scope.hideTweets = false;
        },function(){
            $scope.rateLimitError = true;
        });
    }

    $scope.postTweet = function() {
        twitterService.postTweets($scope.tweet).then(function(data){
            console.log("Post feed success");
            $scope.hideTweets = false;
            $scope.tweets.unshift(data);
            $scope.tweet = null; // reseting value
            $scope.searchTerm = null;
        },function(){
            console.log("Error in post feed");
        })
    }

    //when the user clicks the connect twitter button, the popup authorization window opens
    $scope.connectButton = function() {
        twitterService.connectTwitter().then(function() {
            if (twitterService.isReady()) {
                //if the authorization is successful, hide the connect button and display the tweets
                $('#connectButton').fadeOut(function(){
                    $('#getTimelineButton, #signOut').fadeIn();
                    $scope.refreshTimeline();
					$scope.connectedTwitter = true;
                });
            } else {

			         }
        });
    }

    //sign out clears the OAuth cache, the user will have to reauthenticate when returning
    $scope.signOut = function() {
        twitterService.clearCache();
        $scope.tweets.length = 0;
        $('#getTimelineButton, #signOut').fadeOut(function(){
            $('#connectButton').fadeIn();
			$scope.$apply(function(){$scope.connectedTwitter=false})
        });
        $scope.rateLimitError = false;    
    }

    $scope.deleteTweet = function(t,index) {  
        console.log(t);
        twitterService.deleteTweet(t.id_str).then(function(){
            console.log("Delete feed success");
            $scope.tweets.splice(index,1); // Removing from UI
        },function(){
            console.log("Error in deleting feed");
        })
    };

    $scope.searchTweets = function() {
        twitterService.searchTweets($scope.searchTerm).then(function(data){
            console.log("Got Search Results");
            $scope.searchedTweets = data;  
            $scope.hideTweets = true;
        },function(){
            console.log("Error in deleting feed");
        })  
    }

    //if the user is a returning user, hide the sign in button and display the tweets
    if (twitterService.isReady()) {
        $('#connectButton').hide();
        $('#getTimelineButton, #signOut').show();
     		$scope.connectedTwitter = true;
        $scope.refreshTimeline();
    }

});
