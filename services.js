            angular.module('twitterApp.services', []).factory('twitterService', function($q) {

    var authorizationResult = false;
    var STATUS_UPDATE_URL = '/1.1/statuses/update.json';
    var STATUS_DELETE_URL = '/1.1/statuses/destroy/';
    var SEARCH_TWEETS_URL = "/1.1/search/tweets.json";

  function postRequest(url, neededParams, optionalParams) {
        var deferred = $q.defer();
        if (typeof(optionalParams)==='undefined') optionalParams = {};
        var parameters = angular.extend(optionalParams, neededParams);

        // // Append the bodyparams to the URL
      /*  var t = $twitterHelpers.createTwitterSignature('POST', url, parameters, clientId, clientSecret, token);
        if (parameters !== {}) url = url + '?' + $twitterHelpers.transformRequest(parameters);

        $http.post(url, parameters)
        .success(function(data, status, headers, config) {
          deferred.resolve(data);
        })
        .error(function(data, status, headers, config) {
            if (status === 401) {
              token = null;
            }
            deferred.reject(status);
        });
        return deferred.promise;*/
        if (parameters !== {}) url = url + '?' + transformRequest(parameters);
        var deferred = $q.defer();
        var promise = authorizationResult.post(url,parameters).done(function(data) { //https://dev.twitter.com/docs/api/1.1/get/statuses/home_timeline
            //when the data is retrieved resolve the deferred object
            deferred.resolve(data);
        }).fail(function(err) {
           //in case of any error we reject the promise with the error object
            deferred.reject(err);
        });
        //return the promise of the deferred object
        return deferred.promise;
  }

    return {
        initialize: function() {
            //initialize OAuth.io with public key of the application
            OAuth.initialize('19gVB-kbrzsJWQs5o7Ha2LIeX4I', {cache:true});
            //try to create an authorization result when the page loads, this means a returning user won't have to click the twitter button again
            authorizationResult = OAuth.create('twitter');
        },
        isReady: function() {
            return (authorizationResult);
        },
        connectTwitter: function() {
            var deferred = $q.defer();
            OAuth.popup('twitter', {cache:true}, function(error, result) { //cache means to execute the callback if the tokens are already present
                if (!error) {
                    authorizationResult = result;
                    deferred.resolve();
                } else {
                    //do something if there's an error

                }
            });
            return deferred.promise;
        },
        clearCache: function() {
            OAuth.clearCache('twitter');
            authorizationResult = false;
        },
        getLatestTweets: function (maxId) {
            //create a deferred object using Angular's $q service
            var deferred = $q.defer();
      			var url='/1.1/statuses/user_timeline.json';
      			if(maxId){
      				url+='?max_id='+maxId;
      			}
            var promise = authorizationResult.get(url).done(function(data) { //https://dev.twitter.com/docs/api/1.1/get/statuses/home_timeline
                //when the data is retrieved resolve the deferred object
				        deferred.resolve(data);
            }).fail(function(err) {
               //in case of any error we reject the promise with the error object
                deferred.reject(err);
            });
            //return the promise of the deferred object
            return deferred.promise;
        },
        postTweets : function(statusText,parameters) {
            return postRequest(STATUS_UPDATE_URL, {status: statusText}, parameters);   
        },
        deleteTweet : function(tid) {
            var url = STATUS_DELETE_URL + tid + ".json";
            return postRequest(url);   
        },
         searchTweets: function(keyword) {
             //return getRequest(SEARCH_TWEETS_URL, {q: keyword}, parameters);
             var parameters = {q: keyword};
              //create a deferred object using Angular's $q service
            var deferred = $q.defer();
            var url=SEARCH_TWEETS_URL + '?' + transformRequest(parameters);;
            var promise = authorizationResult.get(url).done(function(data) { //https://dev.twitter.com/docs/api/1.1/get/statuses/home_timeline
                //when the data is retrieved resolve the deferred object
                        deferred.resolve(data);
            }).fail(function(err) {
               //in case of any error we reject the promise with the error object
                deferred.reject(err);
            });
            //return the promise of the deferred object
            return deferred.promise;
        }
    }

}).directive('ngEnter', function() {
        return function(scope, element, attrs) {
            element.bind("keydown keypress", function(event) {
                if(event.which === 13) {
                        scope.$apply(function(){
                                scope.$eval(attrs.ngEnter);
                        });
                        
                        event.preventDefault();
                }
            });
        };
}); 

  function transformRequest(obj) {
      var str = [];
      for(var p in obj)
      str.push(encodeURIComponent(p) + "=" + escapeSpecialCharacters(obj[p]));
      console.log(str.join('&'));
      return str.join('&');
  }
  function escapeSpecialCharacters(string) {
        var tmp = encodeURIComponent(string);
        tmp = tmp.replace(/\!/g, "%21");
        tmp = tmp.replace(/\'/g, "%27");
        tmp = tmp.replace(/\(/g, "%28");
        tmp = tmp.replace(/\)/g, "%29");
        tmp = tmp.replace(/\*/g, "%2A");
        return tmp;
  }

  